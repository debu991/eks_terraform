
variable "vpc_id" {
  default = ""
}

variable "vpc_public_subnets" {
  default = []
}

variable "environment" {
  default = ""
}

variable "stage" {
  default = ""
}

variable "ssh_public_key" {
  default = ""
}

variable "min_size" {
  default = 0
}

variable "max_size" {
  default = 1
}

variable "desired_capacity" {
  default = 0
}

resource "kubernetes_secret" "regcred" {
  metadata {
    name      = "regcred"
    namespace = var.application_namespace
  }

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "${var.registry_server}": {
      "auth": "${base64encode("${var.registry_username}:${var.registry_password}")}"
    }
  }
}
DOCKER
  }

  type = "kubernetes.io/dockerconfigjson"

  depends_on = [
    kubernetes_namespace.namespaces
  ]
}

resource "kubernetes_secret" "aws-config" {
  metadata {
    name      = var.aws_credential_secret_name
    namespace = var.aws_credential_namespace
  }
  type = "Opaque"
  data = {
    aws_access_key_id     = var.aws_access_key_id
    aws_secret_access_key = var.aws_secret_access_key
  }

  depends_on = [
    kubernetes_namespace.namespaces
  ]
}

resource "kubernetes_namespace" "namespaces" {
  count = length(var.k8s_namespaces)
  metadata {
    annotations = {
      name = var.k8s_namespaces[count.index]
    }

    name = var.k8s_namespaces[count.index]
  }
}
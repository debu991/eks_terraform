provider "aws" {
  profile = var.profile
  region  = var.region
}

provider "kubernetes" {
  load_config_file       = "false"
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_caller_identity" "current" {}
data "aws_availability_zones" "available" {}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name   = var.vpc_name
  cidr   = var.vpc_cidr
  azs    = var.vpc_azs
  #azs            = slice(data.aws_availability_zones.azs.names, 0, 2)
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = {
    Stage                                       = var.stage
    Environment                                 = var.environment
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = "1"
  }
}

module "bastion" {
  source             = "./modules/bastion"
  environment        = var.environment
  stage              = var.stage
  vpc_public_subnets = module.vpc.public_subnets
  vpc_id             = module.vpc.vpc_id
  ssh_public_key     = var.ssh_public_key
}

resource "random_password" "rds_default_password" {
  length  = 10
  special = false
}

resource "aws_security_group" "rds-sg" {
  name        = "rds-sg"
  description = "Allows connection to mysql"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "TCP"
    self      = true

    security_groups = [
      "${module.eks.worker_security_group_id}",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


module "rds" {
  source               = "terraform-aws-modules/rds/aws"
  identifier           = "${var.environment}-${var.stage}-rds"
  engine               = "mysql"
  family               = "mysql5.7"
  engine_version       = "5.7.25"
  major_engine_version = "5.7"
  instance_class       = var.rds_instance_class
  allocated_storage    = 5

  name                                = var.rds_default_database
  username                            = var.rds_default_username
  password                            = random_password.rds_default_password.result
  port                                = "3306"
  iam_database_authentication_enabled = false

  vpc_security_group_ids = ["${aws_security_group.rds-sg.id}"]
  subnet_ids             = module.vpc.private_subnets

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  final_snapshot_identifier = "${var.environment}-${var.stage}-rds-backup-final"
  deletion_protection       = false

  storage_encrypted = false
  multi_az          = false

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8"
    }
  ]

  tags = {
    Stage       = var.stage
    Environment = var.environment
  }
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version
  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.private_subnets

  map_users    = var.map_users
  map_accounts = var.map_accounts

  write_kubeconfig = true
  manage_aws_auth  = true
  enable_irsa      = true

  kubeconfig_aws_authenticator_env_variables = {
    AWS_PROFILE = var.profile
  }
  workers_group_defaults = {
    key_name = "bastion_key"
  }

  worker_groups = [
    {
      name                          = "on-demand-1"
      instance_type                 = var.cluster_instance_type
      asg_min_size                  = 0
      asg_desired_capacity          = 0
      asg_max_size                  = 1
      additional_security_group_ids = [module.bastion.bastion_sg_id]
      kubelet_extra_args            = "--node-labels=kubernetes.io/lifecycle=normal"
      suspended_processes           = ["AZRebalance"]
    },
    {
      name                          = "spot-1"
      instance_type                 = var.cluster_instance_type
      asg_desired_capacity          = var.cluster_node_number
      asg_max_size                  = 3
      additional_security_group_ids = [module.bastion.bastion_sg_id]
      kubelet_extra_args            = "--node-labels=node.kubernetes.io/lifecycle=spot"
      suspended_processes           = ["AZRebalance"]
      tags = [
        {
          "key"                 = "k8s.io/cluster-autoscaler/enabled"
          "propagate_at_launch" = "true"
          "value"               = "true"
        },
        {
          "key"                 = "k8s.io/cluster-autoscaler/${var.cluster_name}"
          "propagate_at_launch" = "true"
          "value"               = "true"
        },
        {
          "key"                 = "node-role.kubernetes.io/spot-worker"
          "propagate_at_launch" = "true"
          "value"               = "true"
        }
      ]
    }
  ]
}

resource "aws_iam_policy" "eks-ssm" {
  name = "eks-ssm"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ssm:GetParameter"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ssm-policy-attach" {
  role       = module.eks.worker_iam_role_name
  policy_arn = aws_iam_policy.eks-ssm.arn
}

resource "aws_iam_policy" "eks-autoscaler" {
  name = "eks-autoscaler"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "autoscaler-policy-attach" {
  role       = module.eks.worker_iam_role_name
  policy_arn = aws_iam_policy.eks-autoscaler.arn
}
resource "kubernetes_service_account" "service_account" {
  metadata {
    name      = var.ci_k8s_service_account
    namespace = var.application_namespace
  }
}

resource "kubernetes_cluster_role_binding" "service_account_cluster_role_binding" {
  metadata {
    name = var.ci_k8s_service_account
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "ServiceAccount"
    name      = var.ci_k8s_service_account
    namespace = var.application_namespace
  }
}

resource null_resource "generate_config" {
  #triggers = {
  #  always_run = "${timestamp()}"
  #}

  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    command     = <<EOD
      TOKEN_NAME=`kubectl -n ${var.application_namespace} get serviceaccount/${var.ci_k8s_service_account} -o jsonpath='{.secrets[0].name}'`
      TOKEN=`kubectl -n ${var.application_namespace} get secret $TOKEN_NAME -o jsonpath='{.data.token}'| base64 --decode`
      KUBECONFIG=${path.cwd}/${var.ci_k8s_service_account}.kubeconfig kubectl config set-credentials ${var.ci_k8s_service_account} --token=$TOKEN
EOD
  }
  depends_on = [
    kubernetes_service_account.service_account,
    kubernetes_cluster_role_binding.service_account_cluster_role_binding
  ]
}
resource "aws_s3_bucket" "velero_bucket" {
  bucket = var.velero_bucket
  acl    = "private"
}

data "aws_iam_policy_document" "velero_assume_role" {
  statement {
    sid = "serviceaccount"

    actions = [
      "sts:AssumeRoleWithWebIdentity",
    ]

    principals {
      type        = "Federated"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${replace(module.eks.cluster_oidc_issuer_url, "https://", "")}"]
    }

    condition {
      test     = "StringEquals"
      variable = "${replace(module.eks.cluster_oidc_issuer_url, "https://", "")}:sub"

      values = [
        "system:serviceaccount:${var.velero_namespace}:velero-server",
      ]
    }
  }
}

data "aws_iam_policy_document" "velero_policy" {
  statement {
    sid = "ec2"

    actions = [
      "ec2:DescribeVolumes",
      "ec2:DescribeSnapshots",
      "ec2:CreateTags",
      "ec2:CreateVolume",
      "ec2:CreateSnapshot",
      "ec2:DeleteSnapshot",
    ]

    resources = ["*", ]
  }
  statement {
    sid = "s3list"

    actions = [
      "s3:ListBucket",
    ]

    resources = ["arn:aws:s3:::${var.velero_bucket}", ]
  }

  statement {
    sid = "s3backup"

    actions = [
      "s3:GetObject",
      "s3:DeleteObject",
      "s3:PutObject",
      "s3:AbortMultipartUpload",
      "s3:ListMultipartUploadParts"
    ]
    resources = ["arn:aws:s3:::${var.velero_bucket}/*", ]
  }
}

resource "aws_iam_role" "velero_role" {
  name               = format("%s-%s", var.cluster_name, "velero")
  assume_role_policy = data.aws_iam_policy_document.velero_assume_role.json
}

resource "aws_iam_role_policy" "velero_role_policy" {
  name   = format("%s-%s", var.cluster_name, "velero")
  role   = aws_iam_role.velero_role.id
  policy = data.aws_iam_policy_document.velero_policy.json
}
resource "aws_key_pair" "ssh_access_key" {
  key_name   = "bastion_key"
  public_key = var.ssh_public_key
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_security_group" "bastion_sg" {
  name        = "bastion"
  description = "Allow inbound ssh traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Stage       = var.stage
    Environment = var.environment
    Name        = "Bastion"
  }
}

resource "aws_security_group" "allow_connection_from_bastion_sg" {
  name        = "bastion-inbound"
  description = "Allow inbound ssh traffic from bastion"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_sg.id]
  }

  tags = {
    Stage       = var.stage
    Environment = var.environment
    Name        = "Bastion SecurityGroup"
  }
}

module "bastion-asg" {
  source = "terraform-aws-modules/autoscaling/aws"

  name = "bastion_asg"

  # Launch configuration
  lc_name = "bastion_asg_lc"

  image_id                    = data.aws_ami.ubuntu.id
  instance_type               = "t2.nano"
  key_name                    = aws_key_pair.ssh_access_key.key_name
  associate_public_ip_address = true

  security_groups = [aws_security_group.bastion_sg.id]

  root_block_device = [
    {
      volume_size = "8"
      volume_type = "gp2"
    },
  ]

  # Auto scaling group
  asg_name            = "bastion_asg"
  vpc_zone_identifier = var.vpc_public_subnets
  health_check_type   = "EC2"
  min_size            = var.min_size
  max_size            = var.max_size
  desired_capacity    = var.desired_capacity

  tags = [
    {
      key                 = "Environment"
      value               = var.environment
      propagate_at_launch = true
    },
    {
      key                 = "Stage"
      value               = var.stage
      propagate_at_launch = true
    }
  ]
}
